function [n, V, M] = fit_plane(P)
%
% [n, V, M] = fit_plane(P)
%
% Fit a 2D plane to 3D data
%
% Jerome Boulanger 2016 inspired by affine_fit from Adrien Leygue
M = mean(P,1);
Q = bsxfun(@minus,P,M);
[U,S,V] = svd(Q);
n = V(:,3);
V = V(:,1:2);