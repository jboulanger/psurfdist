function D = pairwise_distance(X,Y)
%
% D = pairwise_distance(X,Y)
%
% Compute the pair wise distance matrix between points X and Y
%
% Jerome Boulanger 2016

D = zeros(size(X,1),size(Y,1));
for i = 1:size(X,1)
    for j = 1:size(Y,1)       
        D(i,j) = norm(X(i,:)-Y(j,:));        
    end
end 