function D = distance_points_to_plane(P,n,M)
%
% D = distance_points_to_plane(P,n,M)
%
% compute the distance between some coordinates P [x,y,z] and a plane
% defined by it normal and point M
%
% Jerome Boulanger 2016

a = -n(1) * M(1) - n(2) * M(2) - n(3) * M(3);
b = norm(n);
D = abs(n(1) * P(:,1) + n(2) * P(:,2) + n(3) * P(:,3) + a) / b;
