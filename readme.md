Measure the distances between points belonging to two flatish surfaces

Fit a plane and rotate the point clouds to that they can be interpolated on a 
finer grid. The distance between the surfaces is the minimim distance between 
the points of the interpolated grid. Mean, min, max and standard deviation are
also computed. This approach avoids the complicated problem of estimated a mesh 
from a point cloud.

Usage
-----
Launch the script 'pm_em_distance' and select a file.

The output is the
- distance between each point of the two sets
- distance between each points and the plane associated to the other set
- distance bewtween points from interpolated surfaces
