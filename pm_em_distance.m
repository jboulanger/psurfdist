% Compute the Distance between two set of points
%
% Usage: Run and select a file and 
%
% Jerome Boulanger 2016 for Patrick Hoffmann
%

clear all; close all
[filename, pathname] = uigetfile('*.txt');

dxyz = 1.102; % Pixel size

fprintf('\nFile: ''%s''\n', filename);

data = dlmread([pathname, filesep, filename]);
% index for the er and pm in the file
er = 2;
pm = 3;

% extract the coordinates from the data
X1 = data(data(:,1)==er, 3) * dxyz;
Y1 = data(data(:,1)==er, 4) * dxyz;
Z1 = data(data(:,1)==er, 5) * dxyz;

X2 = data(data(:,1)==pm, 3) * dxyz;
Y2 = data(data(:,1)==pm, 4) * dxyz;
Z2 = data(data(:,1)==pm, 5) * dxyz;

% compute the distance between point clouds
D0 = min(pairwise_distance([X1,Y1,Z1], [X2,Y2,Z2]));
fprintf(1,'Distance between clouds\n min:%.2f max:%.2f avg:%.2f std:%.2f\n', min(D0), max(D0), mean(D0), std(D0));

% fit a plane and compute the distance of all points to the planes
%[n1,V1,p1] = affine_fit([X1,Y1,Z1]);
%[n2,V2,p2] = affine_fit([X2,Y2,Z2]);
[n1,V1,p1] = fit_plane([X1,Y1,Z1]);
[n2,V2,p2] = fit_plane([X2,Y2,Z2]);
angle = acosd(dot(n1,n2));
if angle > 90
    angle = 180-angle;
end

D1 = distance_points_to_plane([X1,Y1,Z1], n2, p2);
D2 = distance_points_to_plane([X2,Y2,Z2], n1, p1);

fprintf('Angle between planes: %.2fdeg\n', angle)
fprintf(1,'Distance pts 1 & planes2\n min:%.2f max:%.2f avg:%.2f std:%.2f\n', min(D1), max(D1), mean(D1), std(D1));
fprintf(1,'Distance pts 2 & planes1\n min:%.2f max:%.2f avg:%.2f std:%.2f\n', min(D2), max(D2), mean(D2), std(D2));

% Use a smooth interpolation of the surface
% rotate first the clouds using the planes orientation
R = [V1,n1];
center = p1;
ndata1 = (R \ bsxfun(@minus,[X1, Y1, Z1], center)')';
rX1 = ndata1(:,1);
rY1 = ndata1(:,2);
rZ1 = ndata1(:,3);

ndata2 = (R \ bsxfun(@minus,[X2, Y2, Z2], center)')';
rX2 = ndata2(:,1);
rY2 = ndata2(:,2);
rZ2 = ndata2(:,3);

% use the same bounding box for interpolation
n = 20;
xmin = min(min(rX1), min(rX2));
xmax = max(max(rX1), max(rX2));
ymin = min(min(rY1), min(rY2));
ymax = max(max(rY1), max(rY2));
[gX,gY] = meshgrid(linspace(xmin,xmax,n), linspace(ymin, ymax,n));

gZ1 = griddata(rX1,rY1,rZ1,gX,gY,'v4');
gZ2 = griddata(rX2,rY2,rZ2,gX,gY,'v4');

surf(gX,gY,gZ1);zlabel('z');ylabel('y');axis equal
hold on;surf(gX,gY,gZ2);zlabel('z');ylabel('y');axis equal;hold off

% rotate back the interpolated surfaces
gdata1 = bsxfun(@plus,(R * [gX(:), gY(:), gZ1(:)]')',center);
gX1 = reshape(gdata1(:,1),[n n]);
gY1 = reshape(gdata1(:,2),[n n]);
gZ1 = reshape(gdata1(:,3),[n n]);

gdata2 = bsxfun(@plus,(R * [gX(:), gY(:), gZ2(:)]')',center);
gX2 = reshape(gdata2(:,1),[n n]);
gY2 = reshape(gdata2(:,2),[n n]);
gZ2 = reshape(gdata2(:,3),[n n]);

% compute the distance between the points of the two surfaces
C = min(pairwise_distance([gX1(:),gY1(:),gZ1(:)], [gX2(:),gY2(:),gZ2(:)]));
fprintf(1,'Distance pts 1 & surf 2\n min:%.2f max:%.2f avg:%.2f std:%.2f\n', min(C), max(C), mean(C), std(C));

% visualization
% compute the fitted plane
n = 2;
xmin = min(min(X1), min(X2));
xmax = max(max(X1), max(X2));
ymin = min(min(Y1), min(Y2));
ymax = max(max(Y1), max(Y2));
[nX,nY] = meshgrid(linspace(xmin,xmax,n), linspace(ymin, ymax,n));
nZ1 = - (n1(1)/n1(3)*nX+n1(2)/n1(3)*nY-dot(n1,p1)/n1(3));
nZ2 = - (n2(1)/n2(3)*nX+n2(2)/n2(3)*nY-dot(n2,p2)/n2(3));

% plot data and models
plot3(X1,Y1,Z1,'r.')
hold on
plot3(X2, Y2, Z2,'b.')
surf(nX, nY, nZ1,'facecolor','red','facealpha',0.25); 
surf(gX1, gY1, gZ1,'facecolor','red','facealpha',0.5); hold on
surf(nX, nY, nZ2,'facecolor','blue','facealpha',0.25); 
surf(gX2, gY2, gZ2,'facecolor','blue','facealpha',0.5); 
hold off
xlabel('x (nm)'),ylabel('y (nm)'),zlabel('z (nm)')
axis equal
title(filename,'interpreter','none')
legend('ER','PM')